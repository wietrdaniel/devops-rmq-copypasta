.PHONY: clean build run

build:
	pip install -r requirements.txt
	python setup.py install --record installed_files.log

clean:
	python setup.py clean
	rm -rf ./build/
	rm -rf ./dist/
	rm -rf ./RMQ_CopyPasta.egg-info/
	cat installed_files.log | xargs rm -rf
	rm -f installed_files.log

run:
	RMQ_CopyPasta -h
