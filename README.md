RMQ_CopyPasta.py 0.1.0
======================
Terminal application to perform a backup and restore of a RabbitMQ Queue.

* **Backup functionality** : store to disk or perform mirror to other Queue

  Example:
    `./RMQ_CopyPasta.py -u usr -p pwd --vhost test -q example -m`

  > This will create a dump 'Dump-example.pickle' and
  > a mirror-queue example.Backup

* **Restore functionality**: File to RabbitMQ Queue, Queue to mirror Queue

  Example:
    `./RMQ_CopyPasta.py -u usr -p pwd --vhost test -q example -r --file ./dumpfile`

  > This will restore a queue from a dump 'dumpfile' to
  > a the provided queue 'example'

## Installation

Run `make` or `sudo make` to install dependencies and install the console application RMQ_CopyPasta.

To remove run `make clean` or `sudo make clean`, this  will remove the built and installed files.


### Dependency

* pika 0.10.0


Author  : Daniel Wietrzychowski

Date    : 08/06/2017
