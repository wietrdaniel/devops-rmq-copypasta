#!/usr/bin/env python

# RMQ_CopyPasta.py
# ----------------
# Terminal application to perform a backup and restore of a RabbitMQ Queue.
# Backup functionality : store to disk or mirror to other Queue
#   Example: ./RMQ_CopyPasta.py --user user
#                               --password pwd
#                               --vhost test
#                               --queue example
#                               -m
#
#           This will create a dump 'Dump-example.json' and
#           a mirror-queue example.Backup
#
#
# Restore functionality: File to RabbitMQ Queue, Queue to mirror Queue
#   Example: ./RMQ_CopyPasta.py --user user
#                               --password pwd
#                               --vhost test
#                               --queue example
#                               -r
#                               --file ./Dump-example.json
#
#           This will restore a queue from a dump 'Dump-example.json' to
#           a the provided queue 'example'
#
# Dependency: pika v 0.10.1
#
# Author  : Daniel Wietrzychowski
# License : GPLv2
# Date    : 08/06/2017


import pika         # Module for RabbitMQ
import time         # Module for monitoring execution time
import pickle       # Module to store arbitrary Object as String
import base64       # Module to safely store a String
import json         # Module to write file as json
import sys          # Module to write to stdout
import os           # Module to parse memory information
import argparse     # Module to parse passed arguments to terminal application

# Login information

RabbitMQ_Host = "GlobalPlaceholder"
RabbitMQ_User = "GlobalPlaceholder"
RabbitMQ_Pass = "GlobalPlaceholder"
RabbitMQ_Queue = "GlobalPlaceholder"
RabbitMQ_BackQ = "GlobalPlaceholder"
RabbitMQ_xDLX = "DLX"
RabbitMQ_vHost = "GlobalPlaceholder"


class MessageQ:
    """Simple class to temporarily store records in memory"""

    def __init__(self):
        ''' Initialise empty mq '''
        self.list = []
        self.size = 0

    def addMessage(self, properties, body):
        ''' Add message header and body to mq list'''
        # DEPRECATED
        # Properties is an <properties>-object, it cannot be json encoded
        block = {'properties': properties, 'body': body}
        self.list.append(block)

    def pushMessage(self, properties, body):
        ''' push message bottom of mq list, make list json encodable '''
        # Preserve <properties>-object by pickling and encoding in base64
        # If object is encoded as string, json.dump can be used
        pickleprops = pickle.dumps(properties, protocol=2)  # Support Python2.7
        b64props = base64.b64encode(pickleprops)
        block = {'properties': b64props, 'body': body}
        self.list.append(block)

    def popMessage(self):
        ''' pop message from top of mq list'''
        # Unpack <properties>-object decoding base64 and unpickling
        block = self.list.pop(0)
        pickleprops = base64.b64decode(block['properties'])
        properties = pickle.loads(pickleprops)
        return {'properties': properties, 'body': block['body']}

    def setSize(self, count):
        ''' Set reported size to variable '''
        self.size = count

    def getSize(self):
        ''' Return reported size to variable '''
        return self.size

    def GetQ(self):
        ''' Returns list of messages '''
        return self.list

    def fromFile(self, list_obj):
        ''' Lazy replace stored mq, with data from file '''
        self.list = list_obj
        self.size = len(list_obj)


class ProgressCounter:
    ''' Simple Class to display the progress of a loop '''
    def __init__(self, max_expected_value, max_amount_steps=1000):
        ''' init with expected number of itterations '''
        self.MAX_VAL = max_expected_value
        MOD = int(self.MAX_VAL//max_amount_steps)
        self.MOD = MOD if MOD != 0 else 1  # MOD cannot be 0
        self.counter = 0
        self.error_map = {}

    def addError(self, key, value):
        self.error_map[key] = value
        return 0

    def showError(self, keys):
        err = 0
        for k, v in self.error_map:
            if k in keys:
                print(self.error_map[k])
                err = 1
        return err

    def modIsZero(self, counter=None):
        if counter is None:
            counter = self.counter
        return ((counter % self.MOD) == 0)

    def getProgress(self, counter=None):
        if counter is None:
            counter = self.counter
        return round(float(counter) / self.MAX_VAL * 100, 2)

    def displaySelfUpdatingPercentage(self, error_list=[], counter=None):
        # REQUIREMENT: sys module
        if counter is None:
            counter = self.counter
        if self.modIsZero(counter):
            sys.stdout.write("\r%s%%" % self.getProgress(counter))
            self.showError(error_list)
        self.counter += 1


class MemoryUsage:
    ''' Class to prevent running out of memory '''

    def __init__(self, limit=2000000):  # Limit at 2Gb of RAM if free
        free_mem = self.getFreeMemoryInKB()
        LIMIT = limit if limit < free_mem else free_mem
        self.memory_upper_limit = LIMIT
        # Time Loop, Sample and hold method
        self.t0 = time.time()
        self.t1 = self.t0
        self.TIMEOUT = 2  # seconds
        self.my_memory_footprint = 0
        self.MEM_ERR_REGISTERED = False

    def getMemoryFootprintInKB(self):
        ''' Read the last number displayed by the "pmap <my pid>" command '''
        self.t1 = time.time()
        if (self.t1 - self.t0) > self.TIMEOUT:  # refresh every n seconds
            pid = os.getpid()
            cmd = 'pmap %s' % pid
            out = int(os.popen(cmd).readlines()[-1].split()[-1][:-1])
            self.my_memory_footprint = out
            self.t0 = self.t1
        return self.my_memory_footprint

    def getTotalMemoryAvailableInKB(self):
        ''' Get total memory of current node '''
        with open('/proc/meminfo', 'r') as mem:
            total_memory = -1
            for i in mem:
                sline = i.split()
                if str(sline[0]) == 'MemTotal:':
                    total_memory = int(sline[1])
        return int(total_memory)

    def getFreeMemoryInKB(self):
        ''' Get free memory of current node '''
        with open('/proc/meminfo', 'r') as mem:
            free_mem = 0
            for i in mem:
                sline = i.split()
                if str(sline[0]) in ('MemFree:', 'Buffers:', 'Cached:'):
                    free_mem += int(sline[1])
        return int(free_mem)

    def getPercentUse(self):
        ''' Return percentage use as a number between 0 and 1 '''
        mem_used = float(self.getMemoryFootprintInKB())
        limit = self.memory_upper_limit
        result = round(mem_used/limit, 5)
        return result

    def getWarning(self):
        p = self.getPercentUse()
        if p > 1:
            # Raise Exception, usage above limit
            ex = "Memory has reached %sK" % self.getMemoryFootprintInKB()
            ex += "which is above the predefined"
            ex += "boundary of %sK" % self.memory_upper_limit
            raise OutOfMemoryException(ex)
        warningActive = (p >= 0.95)
        self.MEM_ERR_REGISTERED = warningActive
        return warningActive


class OutOfMemoryException(Exception):
    ''' ExceptionClass for raising an OutOfMemoryException '''
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def connectToRabbitMQ(QueueToUse, flags=[]):
    ''' Open a connection to RabbitMQ '''
    # Short flag for debugging
    d = ('debug' in flags)
    u = ('url' in flags)
    x = ('no-dlx' in flags)
    if d:
        print('connect to "%s" as user "%s"' %
              (RabbitMQ_Host, RabbitMQ_User))
    parameters = pika.ConnectionParameters(
        host=RabbitMQ_Host,
        credentials=pika.PlainCredentials(RabbitMQ_User,
                                          RabbitMQ_Pass),
        virtual_host=RabbitMQ_vHost)
    if u:
        parameters = pika.connection.URLParameters(
            'amqps://%s:%s@%s:5672/%s' %
            (RabbitMQ_User,
             RabbitMQ_Pass,
             RabbitMQ_Host,
             RabbitMQ_vHost))
    try:
        connection = pika.BlockingConnection(parameters)
    except:
        if d:
            print("Failed to connect to '%s' with '%s:%s' and vhost '%s'" %
                  (RabbitMQ_Host,
                   RabbitMQ_User,
                   RabbitMQ_Pass,
                   RabbitMQ_vHost))
            print("parameters\n%r" % parameters)
        print("Connection Failed, exiting")
        sys.exit(1)

    if d:
        print('Opening channel')
    channel = connection.channel()
    queue_args = {"x-dead-letter-exchange": RabbitMQ_xDLX,
                  "x-dead-letter-routing-key": QueueToUse}
    if x:
        queue_args = {}
    if d:
        print('Creating Queue "%s"' % QueueToUse)
    try:
        result = channel.queue_declare(queue=QueueToUse,
                                       durable=True,
                                       arguments=queue_args)
    except:
        print("DLX mismatch exception")
        # Closing connection
        connection.close()
        sys.exit(1)
    if d:
        print('Creating Exchange "%s"' % RabbitMQ_xDLX)
    channel.exchange_declare(exchange=RabbitMQ_xDLX,
                             type='direct',
                             durable=True)
    channel.basic_qos(prefetch_count=100)
    # '-> Otherwise algorithm will try to fetch everything
    if d:
        print('Connection established')
    return (connection, channel, result.method.message_count)


def readMQ(mq, memory_usage, flags=[]):
    '''
    Read message queue from RabbitMQ-server and store to memory mq-object
    '''
    # Short debug
    d = ('debug' in flags)
    print('%s Read from RabbitMQ...' % timeUntilPointInCode(flags[-1]))
    if d:
        print("Reading Message Queue")
    connection, channel, record_count = connectToRabbitMQ(RabbitMQ_Queue,
                                                          flags)
    if d:
        print("Initiated connection to %s" % RabbitMQ_Queue)
    mq.setSize(record_count)
    if d:
        print("Will process %s records" % mq.getSize())

    if mq.getSize() == 0:
        # Nothing to do, Queue is empty,
        # continuing may cause blocking behaviour
        print("Empty Queue, abort read")
        if d:
            print("Closing channel and connection")
        channel.close()
        connection.close()
        return 1

    pg = ProgressCounter(mq.getSize())
    for method_frame, properties, body in channel.consume(RabbitMQ_Queue):

        # add the messages to local queue
        mq.pushMessage(properties, body)
        if d:
            print("%i: %s" % (method_frame.delivery_tag, body))

        # Acknowledge the message
        channel.basic_ack(method_frame.delivery_tag)

        # Display Progress
        if not d:
            pg.displaySelfUpdatingPercentage()

        # Check if enough memory is available
        if memory_usage.getWarning():
            if not d:
                print("\r Premature Memory Dump, Memory usage at 95%")
            break

        # Check if TimoutValue is not exceeded
        # Escape out of the loop after messages are read_permission
        if method_frame.delivery_tag >= mq.getSize():
            if not d:
                print("\r 100%   ")
            break

    # Cancel the consumer and return any pending messages
    if d:
        print("Messages processed, request cancel")
    requeued_messages = channel.cancel()

    # Close the channel & connection
    if d:
        print("Closing channel and connection")
    channel.close()
    connection.close()

    print('%s RabbitMQ Queue transfered to memory' %
          timeUntilPointInCode(flags[-1]))
    return 0


def writeMQ(mq, flags=[]):
    ''' Write message queue in memory to RabbitMQ-server '''
    # Short flags
    r = ('retransmit' in flags)
    m = ('mirror' in flags)
    d = ('debug' in flags)
    print('%s Retransmitting to RabbitMQ...' % timeUntilPointInCode(flags[-1]))
    if d:
        print("Writing Message Queue")
    # Open connection & channel for MQ qnd backup MQ
    #   connection_channel = []
    if r:
        connection, channel, _ = connectToRabbitMQ(RabbitMQ_Queue, flags)
        if d:
            print("Connection %s established for retransmission" %
                  RabbitMQ_Queue)
    if m:
        connection2, channel2, _ = connectToRabbitMQ(RabbitMQ_BackQ, flags)
        if d:
            print("Connection %s established for mirror" %
                  RabbitMQ_BackQ)

    if mq.getSize() == 0:
        # Nothing to do, Queue to write is empty, close connections
        if d:
            print("Empty Queue, abort write")
        if r:
            if d:
                print("Closing channel and \
                      connection used for retransmission")
            channel.close()
            connection.close()
        # Close the backup channel & connection
        if m:
            if d:
                print("Closing channel and connection used for mirror")
            channel2.close()
            connection2.close()
        return 1

    if not (r or m):
        return 0  # nothing needs to be transmitted

    pg = ProgressCounter(mq.getSize())  # Progress meter related
    for i in range(len(mq.list)):
        item = mq.popMessage()
        # _ = [basicPublishToRabbitMQ(
        #                             cq[1],
        #                             cq[2],
        #                             item['properties'],
        #                             item['body'])
        #      for cq
        #      in conn_chann]
        if r:
            basicPublishToRabbitMQ(
                                   channel,
                                   RabbitMQ_Queue,
                                   item['properties'],
                                   item['body'])
        if m:
            basicPublishToRabbitMQ(
                                   channel2,
                                   RabbitMQ_BackQ,
                                   item['properties'],
                                   item['body'])
        if d:
            print(item['properties'])
            print(item['body'])
        else:
            pg.displaySelfUpdatingPercentage()
    if not d:
        print("\r 100%   ")

    # Close the channel & connection
    if r:
        if d:
            print("Closing channel and connection used for retransmission")
        channel.close()
        connection.close()
    # Close the backup channel & connection
    if m:
        if d:
            print("Closing channel and connection used for mirror")
        channel2.close()
        connection2.close()

    # _ = [chann[1].close() for chann in conn_chann]
    # _ = [conn[0].close()  for conn  in conn_chann]
    return 0


def basicPublishToRabbitMQ(channel, queue, properties, body):
    '''
    Publish message to RabbitMQ, function to quickly change between channels
    '''
    if channel.is_open:
        channel.basic_publish(exchange='',
                              routing_key=queue,
                              body=body,
                              properties=properties)
    else:
        print(" - - - CHANNEL CLOSED - - - ")
    return 0


def writeToFile(path, data, flags):
    ''' Store json mq-dictionary object to file'''
    print('%s Dumping Queue to file...' %
          timeUntilPointInCode(flags[-1]))
    try:
        with open(str(path), 'wb') as f:
            # f.write(pickle.dumps(data, protocol=2))
            f.write(json.dumps(data))
    except:
        print("Can't write to file: %s" % path)
        sys.exit(1)
    print('%s Dump written to %s' %
          (timeUntilPointInCode(flags[-1]), path))
    return 0


def readFromFile(path, flags):
    '''
    Read dump of json message queue from file and
    return dictionary object
    '''
    print('%s Read from File...' % timeUntilPointInCode(flags[-1]))
    try:
        with open(str(path), 'rb') as f:
            # data = pickle.loads(f.read())
            data = json.loads(f.read())
    except:
        print("Can't open file: %s" % path)
        sys.exit(1)
    print('%s File in memory...' % timeUntilPointInCode(flags[-1]))
    return data


def mergeBatchFiles(filename, counterValue):
    fname = ""
    for i in range(1,counterValue + 1):
        fname = "%s.%i" % (filename, i)
        with open(filename, 'a') as mergeTo, open(fname, 'r') as mergeFrom:
            s = mergeFrom.read()
            if i == 1:  # 1st message, trim trailing bracket
                mergeTo.write("%s, " % (s[:-1]))
            else if i == counterValue + 1:
                # last message, trim leading bracket
                mergeTo.write("%s" % (s[1:]))
            else:
                mergeTo.write("%s, " % (s[1:-1]))


def timeUntilPointInCode(ts):
    ''' Display the execution time for logging purposes '''
    strTimeStamp = '[%10.2f s]' % (time.time() - ts)
    return strTimeStamp


def init_args():
    ''' Parse arguments passed by the commandline '''
    description = 'Terminal application to perform \
                  a backup and restore of a RabbitMQ Queue'
    epilog = ""
    parser = argparse.ArgumentParser(description=description,
                                     epilog=epilog)
    parser.add_argument('-u', '--username',
                        help='RabbitMQ Username',
                        default='guest')
    parser.add_argument('-p', '--password',
                        help='RabbitMQ password',
                        default='guest')
    parser.add_argument('-q', '--queue',
                        help='RabbitMQ queue')
    parser.add_argument('--host',
                        help='RabbitMQ FQDN',
                        default='localhost')
    parser.add_argument('--vhost',
                        help='RabbitMQ Virtual Host',
                        default='/')
    parser.add_argument('--file',
                        help='Pickled dumpfile to restore')
    parser.add_argument('--output',
                        help='Path and filename of the dumpfile')

    parser.add_argument('-r', '--retransmit',
                        help='Retransmit messages to original Queue',
                        action="store_true")
    parser.add_argument('-m', '--mirror',
                        help='Send messages to backup queue',
                        action="store_true")
    parser.add_argument('--debug',
                        help='Debug Mode Enabled',
                        action="store_true")
    parser.add_argument('--url',
                        help='Use URL to connect',
                        action="store_true")
    parser.add_argument('--no-dlx',
                        help='Queue is not marked Dead Letter Exchange',
                        action="store_true")

    parser.add_argument('--version',
                        help='Display version and exit',
                        action="store_true")

    # Checks if no args
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    if args.version:
        print("RMQ_CopyPasta v0.1.1")
        sys.exit(0)

    # Get access to global variables
    global RabbitMQ_Host
    global RabbitMQ_User
    global RabbitMQ_Pass
    global RabbitMQ_Queue
    global RabbitMQ_BackQ
    global RabbitMQ_vHost

    # Edit Global Variables
    RabbitMQ_Host = str(args.host)
    RabbitMQ_User = str(args.username)
    RabbitMQ_Pass = str(args.password)
    RabbitMQ_Queue = str(args.queue)
    RabbitMQ_BackQ = '.'.join([RabbitMQ_Queue, 'Backup'])
    RabbitMQ_vHost = str(args.vhost)

    if args.debug:
        print("Connecting to %s using %s@%s\n" %
              (RabbitMQ_Queue, RabbitMQ_User, RabbitMQ_Host))
    return args


def raiseFlags(args, launchtime):
    ''' Store flags raised in arguments to list '''
    flags = []
    if args.mirror:
        flags.append('mirror')
    if args.retransmit:
        flags.append('retransmit')
    if args.file is not None:
        flags.append('read_from_file')
    if args.debug:
        flags.append('debug')
    if args.url:
        flags.append('url')
    if args.no_dlx:
        flags.append('no-dlx')
    flags.append(launchtime)
    return flags


def backupLoop(args, flags, memory_usage, loopCounter):
    init_timestamp = flags[-1]
    print('%s Starting RMQ_CopyPasta' % timeUntilPointInCode(init_timestamp))
    # Class to save messages in MQ
    mq = MessageQ()
    MemErr = memory_usage.MEM_ERR_REGISTERED
    if not ('read_from_file' in flags):
        # Read from MQ
        readMQ(mq, memory_usage, flags)
        MemErr = memory_usage.MEM_ERR_REGISTERED
        # Store MQ to pickle file
        filename = './%s.dump.json' % RabbitMQ_Queue
        if args.output is not None:
            filename = str(args.output)
        if (MemErr or (loopCounter != 0)):
            filename += ".%i" % (loopCounter + 1)
        writeToFile(filename, mq.GetQ(), flags)
    else:
        mq.fromFile(readFromFile(args.file, flags))

    # Retransmit to original MQ and Backup Queue
    if MemErr:
        return False
    if loopCounter != 0:
        filename = './%s.dump.json' % RabbitMQ_Queue
        if args.output is not None:
            filename = str(args.output)
        mergeBatchFiles(filename, loopcounter)
        for i in range(1, loopCounter + 1):
            tmp_filename = "%s.%i" % (filename, i)
            print('%s resending %s' % (timeUntilPointInCode(init_timestamp),
                  tmp_filename))
            mq.fromFile(readFromFile(tmp_filename, flags))
            writeMQ(mq, flags)
        print('%s Done' % timeUntilPointInCode(init_timestamp))
        return True

    writeMQ(mq, flags)
    print('%s Done' % timeUntilPointInCode(init_timestamp))
    return True


def main():
    ''' main program '''
    init_timestamp = time.time()
    # Parse commandline arguments
    args = init_args()
    flags = raiseFlags(args, init_timestamp)
    memory_usage = MemoryUsage()

    # Loop if memory is more than 95% of the preallocated 2GB

    done = False
    loopCounter = 0
    while not done:
        done = backupLoop(args, flags, memory_usage, loopCounter)
        loopCounter += 1

    return 0


if __name__ == '__main__':
    main()
