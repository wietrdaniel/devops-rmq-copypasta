# -*- coding: utf-8 -*-

from setuptools import setup


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='RMQ_CopyPasta',
    version='0.1.1',
    description='Terminal application to perform a backup and restore of a RabbitMQ Queue.',
    long_description=readme,
    author='Daniel Wietrzychowski',
    author_email='Daniel.wietrzychowski',
    url='https://bitbucket.org/wietrdaniel/devops-rmq-copypasta',
    entry_points={
        'console_scripts':[
            'RMQ_CopyPasta = RMQ_CopyPasta.__main__:main'
        ]
    },
    license='GPL-2.0',
    packages=['RMQ_CopyPasta']
)
